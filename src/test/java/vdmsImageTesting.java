import org.junit.Test;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class vdmsImageTesting {
    List<List<String>> threadData = new ArrayList<>();
    List<Thread> threads = new ArrayList<>();
    int FNR=0; //keeps track of thread data
    int NR=0; // keeps track of string data

    @Test
    public void checkImagesfor500()throws Throwable{
        String filepath = "src/test/resources/fullListVDMS.csv";
        splitFileIntoArrays(filepath);

        for (int i = 0; i <threadData.size() ; i++) {
             threads.add(new ThreadHelper(threadData.get(i)));
        }

        //start all threads
        for (int i = 0; i <threads.size() ; i++) {
          threads.get(i).start();
        }

        //wait for them to join
        for (int i = 0; i <threads.size() ; i++) {
            threads.get(i).join();
        }

        System.out.println("Done!");
    }

    public void splitFileIntoArrays(String filepath) throws Throwable{
        ArrayList<String>  _urls = new ArrayList<>();
        File theFile = new File(filepath);
        Scanner scanner = new Scanner(theFile);
        while(scanner.hasNext()){
            ++NR;
            _urls.add(scanner.next());
            if(NR % 100000 == 0){
                ++FNR;
                threadData.add((List<String>) _urls.clone());   //add it to thread data
                _urls.clear();           //clear _urls to start adding a new list
            }
        }
        threadData.add((List<String>) _urls.clone());  //add the last bit that is left

        scanner.close();
    }

    public HttpURLConnection get(String urlFromFile) throws Throwable{
        URL url = new URL(urlFromFile);
        return  (HttpURLConnection) url.openConnection();
    }

    public class ThreadHelper extends Thread{
        List<String> __urls = new ArrayList<>();

        ThreadHelper(List<String> _urls){
            this.__urls = _urls;
        }

        @Override
        public void run(){
            this.__urls.forEach(x->{
                try{
                    System.out.println(this.getName() +" "+ get(x).getResponseCode());
                    if(get(x).getResponseCode() != 200){
                      System.out.println(x);
                    }
                }
                catch (Throwable e){

                }
            });
        }

    }

}
